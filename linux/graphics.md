# Graphics
* [Nvidia Graphics](#nvidia_graphics)
  * [Nvidia DRM Kernel Mode Setting](#nvidia-drm-kernel-mode-setting)
  * [Load Nvidia Settings on Login](#load-nvidia-settings-on-login)
* [SDL](#sdl)
  * [Define primary display](#define-primary-display)
* [Xorg](#xorg)
  * [Disable Xsession logging](#disable-xsession-logging)

## Nvidia Graphics

### Nvidia DRM Kernel Mode Setting
**Note**: You need a driver of 364.16 or later.

1. Edit the modules file for initramfs:
   ```
   sudoedit /etc/initramfs-tools/modules
   ```

2. Add to the end:
   ```
   # Nvidia DRM Kernel Mode Setting
   nvidia
   nvidia_modeset
   nvidia_uvm
   nvidia_rm
   ```

3. Update initramfs for all kernels:
   ```
   sudo update-initramfs -k all -u
   ```

4. Edit the grub configuration file:
   ```
   sudoedit /etc/default/grub
   ```

5. Find the `GRUB_CMDLINE_LINUX_DEFAULT` variable and add: 
   ```
   nvidia-drm.modeset=1
   ```

6. Update grub:
   ```
   sudo update-grub
   ```

### Load Nvidia Settings on Login
1. Create the file:
   ```
   sudoedit /etc/X11/Xsession.d/90nvidia-custom
   ```

2. Add the following to the new file:
   ```sh
   echo -n "90nvidia-custom: Setting up Nvidia..."
   nvidia-settings -l
   echo "Done"
   ```

## SDL

### Define primary display
1. Edit the conf file:
   ```
   sudoedit /etc/enviroment
   ```

2. Add or change the following line:
   ```
   SDL_VIDEO_FULLSCREEN_DISPLAY=0
   ```

3. Restart the Window Manager

## Xorg

### Disable Xsession logging
**Note**: It's only recommended you disable logging if the log file takes up
too much space.

1. Create the file:
   ```
   sudoedit /etc/X11/Xsession.d/98xsession-disable-logging
   ```
2. Add the following to the new file:
   ```sh
   # Disable logging to "$HOME/.xsession-errors"
   exec > /dev/null 2>&1
   ```

3. Restart the Window Manager
