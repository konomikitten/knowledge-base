# Audio

## PulseAudio

### Disable scaling of application volume sliders

Create the conf.d directory

```sh
sudo mkdir /etc/pulse/daemon.conf.d
```

Create and edit the configuration file

```sh
sudoedit /etc/pulse/daemon.conf.d/local.conf
```

Add the following line

```ini
flat-volumes = no
```

Restart PulseAudio

```sh
sudo systemctl restart pulseaudio
```
