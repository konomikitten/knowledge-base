# Chroots

## Schroot

### Installation

Install schroot

```sh
sudo apt install schroot
```

Create a custom profile directory

```sh
sudo mkdir -p /etc/schroot/custom/
```

Create copyfiles

```sh
sudoedit /etc/schroot/custom/copyfiles
```

```ini
# Files to copy into the chroot from the host system.
#
# <source and destination>
/etc/hosts
/etc/resolv.conf
/etc/networks
```

Create fstab

```sh
sudoedit /etc/schroot/custom/fstab
```

```ini
# fstab: static file system information for chroots.
# Note that the mount point will be prefixed by the chroot path
# (CHROOT_PATH)
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
/proc           /proc           none    rw,bind         0       0
/sys            /sys            none    rw,bind         0       0
/dev/pts        /dev/pts        none    rw,bind         0       0
tmpfs           /dev/shm        tmpfs   defaults        0       0
```

Create nssdatabases

```sh
sudoedit /etc/schroot/custom/nssdatabases
```

```ini
# System databases to copy into the chroot from the host system.
#
# <database name>
passwd
shadow
group
gshadow
```

### Creating a chroot for i386

Create an i386, note replace [USER] with your actual user name

```sh
sudoedit /etc/schroot/chroot.d/i386.conf
```

```ini
type=directory
directory=/srv/chroot/i386
description=i386
message-verbosity=normal
users=[USER]
groups=[USER]
root-users=root
root-groups=root
aliases=
profile=custom
personality=linux32
preserve-environment=false
```

Initialise the chroot, note replace [codename] with your desired debian
version

```sh
sudo mkdir -p /srv/chroot/i386
sudo debootstrap --arch=i386 --include sudo [codename] /srv/chroot/i386 https://deb.debian.org/debian
```

Enter your chroot.

```sh
schroot --chroot=i386
```

Fix up your user home directory, note where [USER] put your actual user name

```sh
sudo mkdir /home/[USER]
sudo chown [USER]:[USER] /home/[USER]
```

### Creating a chroot for amd64

Create an amd64, note replace [USER] with your actual user name

```sh
sudoedit /etc/schroot/chroot.d/amd64.conf
```

```ini
type=directory
directory=/srv/chroot/amd64
description=amd64
message-verbosity=normal
users=[USER]
groups=[USER]
root-users=root
root-groups=root
aliases=
profile=custom
personality=linux
preserve-environment=false
```

Initialise the chroot, note replace [codename] with your desired debian
version

```sh
sudo mkdir -p /srv/chroot/amd64
sudo debootstrap --arch=amd64 --include sudo [codename] /srv/chroot/amd64 https://deb.debian.org/debian
```

Enter your chroot.

```sh
schroot --chroot=amd64
```

Fix up your user home directory, note where [USER] put your actual user name

```sh
sudo mkdir /home/[USER]
sudo chown [USER]:[USER] /home/[USER]
```
