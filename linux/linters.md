# Linters

## Markdown

### Makrdownlint
Markdown lint tool is the only Markdown linter I'm currently aware of. It's
written in Ruby and is generally available through a distribution's
repository. On Debian the package name is `ruby-mdl`. If the package is not
available you need to install via the Ruby gem command the gem is called
`mdl`. See the document on Ruby for details on installing gems.

Configuring Markdownlint can be a bit counterintuitive. Follow the example
provided and then check the rules documentation on the GitHub to customize
the rules.

Create the `~/.mdlrc` file and configure it to point to a rules file.

```rb
style "#{File.expand_path('~/.mdl_style.rb')}"
```

Create the `~/.mdl_style.rb` file and add the following.

```rb
rule 'MD001'
rule 'MD002'
rule 'MD003'
rule 'MD004'
rule 'MD005'
rule 'MD006'
rule 'MD007'
rule 'MD009'
rule 'MD010', :ignore_code_blocks => true
rule 'MD011'
rule 'MD012'
rule 'MD013', :ignore_code_blocks => true
rule 'MD014'
rule 'MD018'
rule 'MD019'
rule 'MD020'
rule 'MD021'
rule 'MD022'
rule 'MD023'
rule 'MD024'
rule 'MD025'
rule 'MD026'
rule 'MD027'
rule 'MD028'
rule 'MD029'
rule 'MD030'
rule 'MD031'
rule 'MD032'
rule 'MD033'
rule 'MD034'
rule 'MD035'
rule 'MD036'
rule 'MD037'
rule 'MD038'
rule 'MD039'
rule 'MD046'
rule 'MD047'
```
