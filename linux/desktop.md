# Desktop

## Default Applications and Associations
* [Editing files](#editing-files)
* [How to change the files](#how-to-change-the-files)
* [Default browser](#default-browser)

### Editing files
#### Users
1. `sudoedit ~/.local/share/applications/mimeapps.list`
2. Make changes to the file.
3. Save the file and the changes should take effect immediately.

#### System
1. `sudoedit /usr/share/applications/defaults.list`
2. Make changes to the file.
3. `sudo update-desktop-database /usr/share/applications/`

### How to change the files
Default applications go under `[Default Applications]` and default associations go under `[Added Associations]`, for example:
```
[Default Applications]
text/plain=geany.desktop;

[Added Associations]
text/plain=mousepad.desktop;
```
You can find a list of `.desktop` files in `/usr/share/applications/`.
You can use `mimetype [file]` to discover the mime type of a file.

You can also check what mime types desktop files support with
`grep 'MimeType=' example.desktop`


### Default browser
1. For apt based distros use `update-alternatives`:
   ```
   sudo update-alternatives --config x-www-browser
   sudo update-alternatives --config gnome-www-browser
   ```

2. Get a generic list of browsers:
   ```
   grep -R -F 'GenericName=Web Browser' /usr/share/applications/
   ```

3. Use your `*.desktop` file of the choice from the previous command
   to set your default browser for mime schemes:
   ```
   xdg-mime default firefox.desktop text/html
   xdg-mime default firefox.desktop x-scheme-handler/http
   xdg-mime default firefox.desktop x-scheme-handler/https
   xdg-mime default firefox.desktop x-scheme-handler/about
   ```

4. You can query what application is the default for a mime type with
   the following command:
   ```
   xdg-mime query default text/html
   ```

5. The changes are stored in your user directory in the following file:
   ```
   ~/.config/mimeapps.list
   ```
