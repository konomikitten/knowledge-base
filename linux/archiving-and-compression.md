# Archiving and compression

## Zip

Compress files

```sh
zip file.zip file1 file2
```

Compress files with compression level (0 Store, 9 Best)

```sh
zip -9 file.zip file1 file2
```

Compress a directory

```sh
zip -r file.zip directory
```

Extract an archive

```sh
unzip file.zip
```

Multipart zips can be used by putting their parts together with cat:

```sh
cat part.01.zip part.02.zip part.03.zip > complete.zip
```

Show a file list of the zip contents

```sh
unzip -l file.zip
```

## Tar

Archive files

```sh
tar -cvf file.tar file1 file2
```

Compress with gzip

```sh
tar -czvf file.tar.gz file1 file2
```

Compress with bzip2

```sh
tar -cjvf file.tar.bz2 file1 file2
```

Compress files with compression level (0 Store, 9 Best)

```sh
XZ_OPT=-9 tar -cJvf file.tar.xz file1 file2
```

Compress files with compression level (0 Store, 9 Best) and root as owner

```sh
XZ_OPT=-9 tar --owner=0 --group=0 -cJvf file.tar.xz file1 file2
```

Extract an archive

```sh
tar -xvf file.tar.xz
```

## 7zip

Compress files with compression level (0 Store, 9 Best)

```sh
7z a -mx=9 file.7z file1 file2
```

Compress files with compression level (0 Store, 9 Best) and limit to 4 threads

```sh
7z a -mmt=4 -mx=9 file.7z file1 file2
```

Compress files with a password and hidden file names

```sh
7z a -p -mhe file.7z file1 file2
```

Extract an archive

```sh
7z x file.7z
```

Extract an archive to specific path

```sh
7z x -oc:$HOME/Documents/Videos/ file.7z
```

## Rar

Extract an archive

```sh
unrar x file.rar
```
