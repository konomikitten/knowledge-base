# systemd
* [Override power keys](#override-power-keys)
* [Persistent logs and limits](#persistent-logs-and-limits)

## Override power keys
1. Create the conf.d directory for logind
   ```
   sudo mkdir /etc/systemd/logind.conf.d/
   ```

2. Create the file and add the following
   ```
   sudoedit /etc/systemd/logind.conf.d/override-power-keys.conf
   ```
   ```
   [Login]
   HandlePowerKey=ignore
   HandleSuspendKey=ignore
   HandleHibernateKey=ignore
   HandleLidSwitch=ignore
   ```

3. Reload systemd
   ```
   systemctl daemon-reload
   ```

## Persistent logs and limits
1. Create the conf.d directory for journald
   ```
   sudo mkdir /etc/systemd/journald.conf.d/
   ```

2. Create the file and add the following
   ```
   sudoedit /etc/systemd/journald.conf.d/persistent-and-limits.conf
   ```
   ```
   [Journal]
   Storage=persistent
   Compress=yes
   SystemMaxUse=1024M
   RuntimeMaxUse=128M
   ```

3. Reload systemd
   ```
   systemctl daemon-reload
   ```
