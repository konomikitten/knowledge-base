# Ecryptfs automated mount

Note: This no longer works as of Debian Bullseye and possibly other modern distros.

1. Install ecryptfs-utils
   ```
   sudo apt install ecryptfs-utils
   ```

2. Create your directories
   ```
   mkdir ~/.ecryptfs-cipher ~/.ecryptfs-plain ~/.ecryptfs
   ```

3. Create your conf file
   ```
   echo "$HOME/.ecryptfs-cipher $HOME/.ecryptfs-plain ecryptfs" > ~/.ecryptfs/ecryptfs-user.conf
   ```

4. Generate your cipher key
   ```
   printf "%s\n" $(od -x -N 100 --width=30 /dev/random | head -n 1 | sed "s/^0000000//" | sed "s/\s*//g")
   ```

5. Generate your wrapped password file (Use your Cipher key for the Password 
   and your Login key for the Wrapper Password)
   ```
   ecryptfs-wrap-passphrase /home/$USER/.ecryptfs/ecryptfs-user.wrapped
   ```

6. Insert your wrapped password into the key-ring and get the signature 
   it produces for the next step
   ```
   ecryptfs-insert-wrapped-passphrase-into-keyring /home/$USER/.ecryptfs/ecryptfs-user.wrapped
   ```

7. Create your signature file (Replace both instance of SIG with the 
   signature you get from doing step 5
   ```
   echo -e "SIG""\n""SIG" > /home/$USER/.ecryptfs/ecryptfs-user.sig
   ```

8. Set up PAM to do auto-mounting
   ```
   echo -e 'auth\t\toptional\tpam_exec.so seteuid expose_authtok log=/etc/pam_exec/ecryptfs.log /etc/pam_exec/ecryptfs.sh' | sudo tee -a /etc/pam.d/common-auth
   ```

   ```
   echo -e 'session\t\toptional\tpam_exec.so seteuid expose_authtok log=/etc/pam_exec/ecryptfs.log /etc/pam_exec/ecryptfs.sh' | sudo tee -a /etc/pam.d/common-session
   ```

9. Set up your shell script for PAM to call (REPLACE YOURUSERNAME with 
   your user name)
   ```
   sudo mkdir /etc/pam_exec/ && sudo touch /etc/pam_exec/ecryptfs && sudo chmod +x /etc/pam_exec/ecryptfs && sudo nano /etc/pam_exec/ecryptfs
   ```

   ```shell
   #!/bin/bash -x

   ECRYPTFS_USER='strawberry'
   ECRYPTFS_ALIAS='ecryptfs-user'
   ECRYPTFS_PASSWORD="/home/$ECRYPTFS_USER/.ecryptfs/$ECRYPTFS_ALIAS.wrapped"
   ECRYPTFS_CONF="/home/$ECRYPTFS_USER/.ecryptfs/$ECRYPTFS_ALIAS.conf"

   if [ "$PAM_USER" == "$ECRYPTFS_USER" ]; then
     # test if authing and test if the mount point is not mounted
     if [ "$PAM_TYPE" == "auth" ] \
     && ! mountpoint -q "$(awk '{ print $2 }' $ECRYPTFS_CONF)"; then
       # insert the wrapped password and mount the alias,
       # expose_authtok types the password into the
       # ecryptfs-insert-wrapped-passphrase-into-keyring command
       # for us
       ecryptfs-insert-wrapped-passphrase-into-keyring \
       "$ECRYPTFS_PASSWORD" && /sbin/mount.ecryptfs_private \
       "$ECRYPTFS_ALIAS"
     fi
     # test if the session is being closed and if the mount point is
     # mounted
     if [ "$PAM_TYPE" == "close_session" ] \
     && mountpoint -q "$(awk '{ print $2 }' $ECRYPTFS_CONF)"; then
       /sbin/umount.ecryptfs_private "$ECRYPTFS_ALIAS"
     fi
   fi
   ```
