# USB
* [Unable to enumerate device](#unable-to-enumerate-device)
* [Show the usb driver used](#show-the-usb-driver-used)
* [Safetly eject devices from the terminal](#safetly-eject-devices-from-the-terminal)

## Unable to enumerate device

### Latest method
1. Create a conf file in the modprobe.d directory:
   ```
   sudoedit /etc/modprobe.d/usb-hci-order.conf
   ```
2. Add the follwing to the file:
   ```
   # Enforce module loading order
   # xhci_hcd, ehci_hcd, ohci_hcd | uhci_hcd

   softdep ehci_hcd pre: xhci_hcd
   softdep uhci_hcd pre: ehci_hcd
   softdep ohci_hcd pre: ehci_hcd
   ```
3. Update initramfs:
   ```
   sudo update-initramfs -u
   ```
4. Reboot the computer.

### Legacy method
1. Create a conf file in the modprobe.d directory:
   ```
   sudoedit /etc/modprobe.d/usb-hci-order.conf
   ```
2. Add the follwing to the file:
   ```
   # Enforce module loading order
   # xhci_hcd, ehci_hcd, ohci_hcd | uhci_hcd

   install ehci_hcd /sbin/modprobe xhci_hcd ; /sbin/modprobe --ignore-install ehci_hcd $CMDLINE_OPTS
   install ohci_hcd /sbin/modprobe ehci_hcd ; /sbin/modprobe --ignore-install ohci_hcd $CMDLINE_OPTS
   install uhci_hcd /sbin/modprobe ehci_hcd ; /sbin/modprobe --ignore-install uhci_hcd $CMDLINE_OPTS
   ```
3. Update initramfs:
   ```
   sudo update-initramfs -u
   ```
4. Reboot the computer.


## Show the usb driver used
1. Run the lsusb tool in tree mode:
   ```
   lsusb -t
   ```
2. If the device shows `Driver=usb-storage` you're using the older driver
   if the device shows `Driver=uas` you're using the more modern driver.


## Safetly eject devices from the terminal
### Ejecting optical media
1. Use the eject command:
   ```
   sudo eject /dev/sdX
   ```

### Ejecting block devices
1. Using udisksctl:
   ```
   sudo udisksctl power-off -b /dev/sdX
   ```
2. Using sysfs:
   ```
   echo offline | sudo tee /sys/block/sdX/device/state
   echo 1 | sudo tee /sys/block/sdX/device/delete
   ```
