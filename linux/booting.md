# Booting

## GRUB

### Changing GRUB's Wallpaper

It's best to use an indexed png image for the wallpaper.

Copy your image to the grub directory, for example:

```sh
cp ~/wallpaper.png /boot/grub/grub-background.png
```

And update the grub.cfg.

```sh
sudo update-grub
```

### Changing GRUB's Font Colours

Edit or create the custom.cfg file.

```sh
sudoedit /boot/grub/custom.cfg
```

Add the following to the file and adjust the colors to your liking.

```cfg
set color_highlight=black/light-gray
set color_normal=light-gray/black
set menu_color_highlight=black/light-gray
set menu_color_normal=light-gray/black
```

[Configure Console Menu Colors](https://wiki.debian.org/GRUB2#Configure_console_menu_colors)
