# Kernel

## Building a vanilla kernel on Debian

Pull in the build dependencies for the Linux kernel

```sh
sudo apt build-dep linux
```

Set the linux version

```sh
export LINUX_VERSION=5.4.113
```

Get the tarball and signing files

```sh
wget -c https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-${LINUX_VERSION}.tar.xz
wget -c https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-${LINUX_VERSION}.tar.sign
```

Get the gpg signing keys for the kernel and verify the tarball is signed
correctly

```sh
gpg --locate-keys torvalds@kernel.org gregkh@kernel.org
xz -cd linux-${LINUX_VERSION}.tar.xz | gpg --verify linux-${LINUX_VERSION}.tar.sign -
```

Extract the tarball and switch to the kernel directory

```sh
tar -xaf linux-${LINUX_VERSION}.tar.xz
cd linux-${LINUX_VERSION}
```

Create the .config file (reuse it from the official package)

```sh
cp /boot/config-5.4.109 .config
```

Edit .config find CONFIG_SYSTEM_TRUSTED_KEYS change it to empty string ""

```sh
editor .config
```

Update the old config

```sh
make oldconfig
```

Disable module signing and creation of the debugging package

```sh
scripts/config --disable MODULE_SIG
scripts/config --disable DEBUG_INFO
```

Set your Debian details

```sh
export DEBFULLNAME='Anonymous'
export DEBEMAIL='anonymous@example.com'
```

Set the kernel details

```sh
export KBUILD_BUILD_USER='debian'
export KBUILD_BUILD_HOST='debian'
```

Clean and build the package

```sh
make clean
make -j $((`nproc`/2)) deb-pkg
```

Install the packages

```sh
sudo dpkg -i linux-headers-5.4.109_5.4.109-1_amd64.deb
linux-image-5.4.109_5.4.109-1_amd64.deb
linux-libc-dev_5.4.109-1_amd64.deb
```

Source: debian-kernel-handbook
