# Environment Variables

Environment variables provide a way to share data between different programs.
They are typically inherited from their parent process. Using these variables
should be a simple process but can be anything but when it comes to the
variance in behaviour between different programs and different Linux
distributions. This document is written from the perspective of the Debian
distribution and may need adjustment for other distributions.


## Configuration Files

### Linux Pluggable Authentication Modules (PAM)

PAM provides pam_env and is the only way I am currently aware of to set
global variables in any reliable and consistent way. PAM will set any
environment variables placed in `/etc/environment` for the entire system.
This method only supports a simple method of `VARIABLE=VALUE`, you can use
`#` for commenting.

There was a per user method of setting variables with PAM but it was disabled
due to security concerns.

### Shell initilization with profiles

For global variables put your `.sh` files into the `/etc/profile.d`
directory. Create this directory if necessary. For per user variables edit
`~/.profile`.

Profile files should be Bourne Shell compatible which follows the POSIX
standard for shells scripts. Note that profiles are only sourced for login
shells.

Most Graphic Environments will not source profiles. If you're using a
graphical environment it's recommended you use Xsession to source your
profiles. See the Xorg Xsession section for more details.

### Xorg Xsession

For global variables edit `/etc/X11/Xsession.d/25custom_local`. For per user
variables edit `~/.xsessionrc`. These files generally do not exist and need
to be created. Just like profiles these files should be Bourne Shell
compatible which follows the POSIX standard for shells scripts.

It's recommended you put environment variables you want to set into Shell
Profiles instead of Xsession and then have Xsession source your profile. Edit
`~/.xsessionrc` and add the following code:

```sh
# Source ~/.profile if it's readable, this enables the shell profile to be
# available in Xorg
if [ -r ~/.profile ]; then
    . ~/.profile
fi
```

### Shells

A lot of shells provide there own files for setting variables, you should
avoid setting environment variables here and stick to using profiles which
affects the majority of shells you'll be using.


## Other ways that may not work

This is provided for informational purposes only. It's recommended you do not
try using these methods as they can be distribution specific and will not
work on every distribution.

### Xprofile

`~/.xprofile` will supposedly be sourced when running Xorg but I have so far
not seen this method work.

### Systemd Environment.d

Systemd will only apply environment variables to services it starts for
users. Environment.d will never apply environment variables to system
services or any other processes not started by a user. This will miss many
processes and makes this method inconsistent for setting environment
variables.

Configuration files (`.conf`) placed in `/etc/environment.d/` will apply to
all systemd user services. Configuration files placed in
`~/.config/environment.d/` will apply to that specific user only.

You can see the environment for your user services by running `systemctl
--user show-environment` and a list of processes these environment variables
are applied to by running `systemctl --user status`. If the process is not in
that list the environment variable will not apply to it.

Environment.d lacks any scripting but allows some forms of parameter
expansion, see `man environment.d` for more details. Variables are set with
`VARIABLE=VALUE`.
