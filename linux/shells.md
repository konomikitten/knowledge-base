# Shells

## Bash

### Disable or enable history for the current shell

Disable history

```sh
set +o history
```

Enable history

```sh
set -o history
```
