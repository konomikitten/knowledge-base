# Ruby Gems

## Install a gem in the user's home directory

```sh
gem install --user-install [gem]
```

## Add user's home bin directory to the PATH enviroment variable

Add the following to `~/.profile`.

```sh
# Set PATH to include user install path for Ruby Gems
if [ -x /usr/bin/ruby ]; then
  GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
  export GEM_HOME
  if [ -d "$GEM_HOME/bin" ]; then
    PATH="$GEM_HOME/bin:$PATH"
  fi
fi
```
