# Debian

## Apt

### Find the source package for a binary package or vice versa

```sh
apt showsrc package
```

### Show all dependencies for a package

```sh
apt depends <package>
```

## Aptitude

### Find manually installed packages with safe guards

```sh
aptitude search '
  ?installed,
  ?not(
    ?or(
      ?automatic,
      ?essential,
      ?priority(required),
      ?priority(important),
      ?priority(standard)
    )
  )
'
```

### Find packages installed by default for Debian

Show the packages installed by debootstrap

```sh
aptitude search '
  ?architecture(native),
  ?version(CURRENT),
  ?or(
    ?priority(required),
    ?priority(important)
  )
'
```

Add standard to show the packages installed by debootstrap and tasksel

```sh
aptitude search '
  ?architecture(native)
  ?version(CURRENT)
  ?or(
    ?priority(required),
    ?priority(important),
    ?priority(standard)
  )
'
```

[Source](https://unix.stackexchange.com/a/90533)

### Find packages not of the current version

```sh
aptitude -F '%c%a%M %p %t %v %V' search '?narrow(?installed, ?not(?archive(unstable)))'
```

### Install package and immediately mark it as automatically installed

```sh
sudo aptitude install package+M
```

[Source](https://www.debian.org/doc/manuals/aptitude/rn01re01.en.html#idm8357)

## Packaging

### Editing a package

Extract the package to a directory called package-temp

```
dpkg-deb --raw-extract package.deb package-temp/
```

Create a new package from the edited package contents

```
dpkg-deb --root-owner-group --build package-temp/ package-new.deb
```

### Fixing all packages being marked as manually installed

If you find that all your packages are manually marked as installed or you
want to clean up the packages on your Debian installed follow these steps.

You can determine if all your packages are marked by running the following
command. The command will show the amount of packages you have as
automatically installed. If it's a low number you may need to follow the rest
of this guide.

```sh
aptitude search "?installed ?automatic" | wc -l
```

This will find every package that is installed manually and it will check if
any of these packages have at least dependency on any other package and if so
mark them as automatically installed.

```sh
aptitude markauto "?installed \
  ?not(?automatic) \
  ?or(
    ?reverse-depends(?installed), \
    ?reverse-predepends(?installed), \
    ?reverse-recommends(?installed), \
    ?reverse-depends(?reverse-depends(?installed)), \
    ?reverse-predepends(?reverse-predepends(?installed)), \
    ?reverse-recommends(?reverse-recommends(?installed)) \
  )"
```

Mark essential packages and packages with the priorities of required,
important and standard. These packages should be always marked as manually
installed.

```sh
aptitude unmarkauto "?installed \
  ?automatic
  ?or(
    ?essential, \
    ?priority(required), \
    ?priority(important), \
    ?priority(standard) \
  )"
```

After this you need to carefully go through the packages you want to keep. Do
not let aptitude or apt remove packages you rely on. If you see either trying
to remove packages you know you want to keep stop the process and mark those
packages as manual, below is an example of packages you might want to keep.

```sh
aptitude unmarkauto libreoffice xorg task-lxqt-desktop openssh-client \
xfonts-terminus
```

Tell aptitude to cancel any pending actions and keep all the current
packages:

```sh
aptitude keep-all
```

### Remove debian themeing

```sh
sudo apt purge desktop-base
```

### Install most documentation packages

```sh
sudo apt install debian-faq debian-refcard debian-handbook \
debian-reference harden-doc debian-policy developers-reference \
debmake-doc maint-guide packaging-tutorial linux-doc
```

