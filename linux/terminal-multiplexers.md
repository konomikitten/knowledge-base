# Terminal multiplexers
* [GNU Screen](#gnu-screen)
  * [Getting Started](#getting-started)
  * [GNU Screen behaves oddly when Weechat runs](#gnu-screen-behaves-oddly-when-weechat-runs)

## GNU Screen

### Getting-Started
1. Install screen
   ```
   sudo apt install screen
   ```

2. Create a custom screen command
   ```
   editor ~/.bash_functions
   ```
   ```sh
   screen-custom() {
     if screen -ls custom &>"/dev/null"; then
       screen -r custom
     elif [ -f ~/.screenrc-custom ]; then
       screen -S custom -c ~/.screenrc-custom
     fi
   }
   ```

3. Create the custom conf file
   ```
   editor ~/.screenrc-custom
   ```
   ```
   #
   # .screenrc file
   #

   # Turn welcome message off
   startup_message off
   # Turn on utf8
   utf8 on
   # Turn on utf8 for new windows
   defutf8 on
   # do not show residual programs in buffer
   altscreen on
   # 256 color support
   term xterm-256color
   # Turn off visual bell
   vbell off

   # Show a statusbar
   hardstatus alwayslastline '%{= .b}[ %{= .w}%-w%{= wk}%50> %n %t %{-}%+w %-01=%{= .b}]'

   # Layout
   screen
   title downloads
   stuff 'cd ~/Downloads'\n
   stuff 'clear'\n

   screen
   title documents
   stuff 'cd ~/Documents'\n
   stuff 'clear'\n

   screen

   select downloads

   # vim: syntax=screen:
   ```

### GNU Screen behaves oddly when Weechat runs
This is due to GNU Screen not handling bracketed pasing correcting.
1. Disable bracketed paste in Weechat
   ```
   /set weechat.look.paste_bracketed off
   ```
