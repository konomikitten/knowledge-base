# Wine

## Remove wine desktop and mime files from user directory

```sh
find ~/.local/share/applications/ -type f \
     -name '*wine-extension-*.desktop' -exec trash '{}' \;
update-desktop-database ~/.local/share/applications/
find ~/.local/share/mime/ -type f \
     -name '*wine-extension-*.xml' -exec trash '{}' \;
update-mime-database ~/.local/share/mime/
```

## Specific Program Fixes

### System Shock 2

Use winetricks to install openal.

Use winecfg and set `ir50_32` to `(native, builtin)`.
