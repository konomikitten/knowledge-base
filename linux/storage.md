# Storage

## Optical

### Listing and Changing the speed of an optical drive

Listing the speed.

```sh
eject --listspeed /dev/sr0
```

Changing the speed.

```sh
eject --cdspeed 16 /dev/sr0
```

### Saving an ISO image

Use iso isoinfo to find the block and volume size.

```sh
isoinfo -d -i /dev/sr0 | awk '/block size|Volume size/'
```

Use those values to run dd.

```sh
dd if=/dev/sr0 of=discmage.iso bs=block_size count=volume_size status=progress
```

### Saving audio tracks

Some distrobutions still package cdparanoia which is no longer maintained.
Be sure to install cd-paranoia (note the dash in the command).

```sh
cd-paranoia --verbose --log-summary cd-paranoia_summary.log \
            --log-debug cd-paranoia_debug.log --output-wav \
            --batch --abort-on-skip
```

## File System Cloning

1. Get a live bootable version of your current distro and boot your PC
   with it

2. Use fdisk to identify the new disk you have added to your system
   ```
   fdisk -l
   ```

   Example output
   ```
   Disk /dev/sda: 16 GiB, 17179869184 bytes, 33554432 sectors
   Disk model: VBOX HARDDISK
   Units: sectors of 1 * 512 = 512 bytes
   Sector size (logical/physical): 512 bytes / 512 bytes
   I/O size (minimum/optimal): 512 bytes / 512 bytes
   Disklabel type: dos
   Disk identifier: 0xb0fe9049

   Device     Boot Start      End  Sectors Size Id Type
   /dev/sda1        2048 33554431 33552384  16G 83 Linux


   Disk /dev/sdb: 8 GiB, 8589934592 bytes, 16777216 sectors
   Disk model: VBOX HARDDISK
   Units: sectors of 1 * 512 = 512 bytes
   Sector size (logical/physical): 512 bytes / 512 bytes
   I/O size (minimum/optimal): 512 bytes / 512 bytes
   ```

3. Run fdisk on your new drive and create the partitions
   ```
   fdisk /dev/sdb
   ```

   Example output
   ```
   Welcome to fdisk (util-linux 2.36.1).
   Changes will remain in memory only, until you decide to write them.
   Be careful before using the write command.

   Device does not contain a recognized partition table.
   Created a new DOS disklabel with disk identifier 0xde49961f.

   Command (m for help): n
   Partition type
      p   primary (0 primary, 0 extended, 4 free)
      e   extended (container for logical partitions)
   Select (default p): p
   Partition number (1-4, default 1):
   First sector (2048-16777215, default 2048):
   Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-16777215, default 16777215):

   Created a new partition 1 of type 'Linux' and of size 8 GiB.

   Command (m for help): w
   The partition table has been altered.
   Calling ioctl() to re-read partition table.
   Syncing disks.
   ```

4. Format your new partition with your file system of choice, the example
   shows btrfs
   ```
   sudo mkfs.btrfs -R free-space-tree /dev/sdb1
   ```

   Example output
   ```
   btrfs-progs v5.11.1
   See http://btrfs.wiki.kernel.org for more information.

   Label:              (null)
   UUID:               313d1bc8-c11c-48c8-9a7b-91acf2c35df1
   Node size:          16384
   Sector size:        4096
   Filesystem size:    8.00GiB
   Block group profiles:
     Data:             single            8.00MiB
     Metadata:         DUP             256.00MiB
     System:           DUP               8.00MiB
   SSD detected:       no
   Incompat features:  extref, skinny-metadata
   Runtime features:   free-space-tree
   Checksum:           crc32c
   Number of devices:  1
   Devices:
      ID        SIZE  PATH
       1     8.00GiB  /dev/sdb1
   ```

5. Use blkid to identify your hard drives
   ```
   blkid
   ```

   Example output
   ```
   /dev/sda1: UUID="4a355767-f6b5-49e8-8f4e-9b0c76aae364" UUID_SUB="b52e5a78-55fc-4af3-83da-4b0392982c4e" BLOCK_SIZE="4096" TYPE="btrfs" PARTUUID="b0fe9049-01"
   /dev/sdb1: UUID="313d1bc8-c11c-48c8-9a7b-91acf2c35df1" UUID_SUB="b094027d-7062-47ef-a228-f75ffa569e6d" BLOCK_SIZE="4096" TYPE="btrfs" PARTUUID="54c3695b-01"
   ```

6. Mount your source and your destination
   ```
   mkdir /mnt/src /mnt/dst
   mount -t btrfs /dev/sda1 /mnt/src
   mount -t btrfs /dev/sdb1 /mnt/dst
   ```

7. Use rsync to copy all your files from the source to the destination
   ```
   rsync -aHAXSh --append-verify --info=progress2,stats2 /mnt/src/ /mnt/dst/
   ```

8. Now change root (chroot) to the destination
   ```
   cd /mnt/dst
   mount -t proc /proc proc/
   mount -t sysfs /sys sys/
   mount -o bind /dev dev/
   chroot . /bin/bash
   ```

9. Insert the file system UUID into the fstab file
   ```
   blkid /dev/sdb1 | tee -a /etc/fstab
   ```

10. Edit the fstab and move the UUID to the root mount
    ```
    nano /etc/fstab
    ```

    The fstab file will look like this when you open it
    ```
    # /etc/fstab: static file system information.
    #
    # Use 'blkid' to print the universally unique identifier for a
    # device; this may be used with UUID= as a more robust way to name devices
    # that works even if disks are added and removed. See fstab(5).
    #
    # <file system>                           <mount point>   <type> <options> <dump> <pass>
    UUID=1a403d2f-f3a8-42ff-ab31-03a74353b238 /               btrfs  defaults  0      0
    /dev/sdb1: UUID="313d1bc8-c11c-48c8-9a7b-91acf2c35df1" UUID_SUB="b094027d-7062-47ef-a228-f75ffa569e6d" BLOCK_SIZE="4096" TYPE="btrfs" PARTUUID="54c3695b-01"
    ```

    After you finish editing fstab should look like this
    ```
    # /etc/fstab: static file system information.
    #
    # Use 'blkid' to print the universally unique identifier for a
    # device; this may be used with UUID= as a more robust way to name devices
    # that works even if disks are added and removed. See fstab(5).
    #
    # <file system>                           <mount point>   <type> <options> <dump> <pass>
    UUID=313d1bc8-c11c-48c8-9a7b-91acf2c35df1 /               btrfs  defaults  0      0
    ```

11. Reconfigure grub making sure to install grub on the new drive in this
    case `sdb`
    ```
    dpkg-reconfigure grub-pc
    ```

12. Exit the chroot and unmount the mounts
    ```
    exit
    umount {proc,sys,dev}
    ```

13. Unmount all the partitions you mounted
    ```
    umount /mnt/{src,dst}
    ```
14. Reboot
    ```
    systemctl reboot
    ```
