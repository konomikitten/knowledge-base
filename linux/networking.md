# Networking

## TCP Ping

Use syn packages to ping a host port via port 443.

```sh
sudo hping3 --quiet --interval 1 --syn --count 10 --destport 443 hostname
```

## systemd DNS resolver

Install:

```sh
sudo apt install systemd-resolved
```

Create the configuration file:

```sh
sudo mkdir /etc/systemd/resolved.conf.d/
sudo touch /etc/systemd/resolved.conf.d/50-local.conf
```

Edit the configuration file:

```sh
sudoedit /etc/systemd/resolved.conf.d/50-local.conf
```

Add the following:

```conf
[Resolve]
DNS=9.9.9.9
FallbackDNS=
Domains=~.
LLMNR=no
MulticastDNS=no
DNSSEC=yes
DNSOverTLS=yes
Cache=yes
CacheFromLocalhost=no
DNSStubListener=no
DNSStubListenerExtra=127.0.0.1:53
DNSStubListenerExtra=[::1]:53
ReadEtcHosts=yes
ResolveUnicastSingleLabel=no
```

Restart the systemd-resolved service

```sh
sudo systemctl restart systemd-resolved
```

Install nftables

```sh
sudo aptitude install nftables
```

Restart the nftables service to ensure the default rules are loaded:

```sh
sudo systemctl restart nftables
```

Run the following nft commands:

```sh
sudo nft add table ip dns_nat
sudo nft add chain ip dns_nat OUTPUT { type nat hook output priority -100\; policy accept\; }
sudo nft add rule ip dns_nat OUTPUT udp dport 53 counter dnat to 127.0.0.1:53
sudo nft add rule ip dns_nat OUTPUT tcp dport 53 counter dnat to 127.0.0.1:53
sudo nft add table ip6 dns_nat
sudo nft add chain ip6 dns_nat OUTPUT { type nat hook output priority -100\; policy accept\; }
sudo nft add rule ip6 dns_nat OUTPUT udp dport 53 counter dnat to [::1]:53
sudo nft add rule ip6 dns_nat OUTPUT tcp dport 53 counter dnat to [::1]:53
```

Save your changes to the nftables configuration file:

```sh
sudo nft list table ip dns_nat | sudo tee -a /etc/nftables.conf
sudo nft list table ip6 dns_nat | sudo tee -a /etc/nftables.conf
```
