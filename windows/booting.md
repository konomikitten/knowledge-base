# Booting

  * [bootrec /fixboot access is denied](#bootrec-fixboot-access-is-denied)
  * [UEFI/GPT hard drive partitions](#UEFIGPT-hard-drive-partitions)
  * [BIOS/MBR hard drive partitions](#BIOSMBR-hard-drive-partitions)

### bootrec /fixboot access is denied

  1. From Recovery go to Command Prompt

  2. Use diskpart to select the disk:
  ````
  diskpart 
  list disk
  select disk #
  ````

  3. Here is an example oh what a disk will look like:
  ````
  DISKPART> list volume

    Volume ###  Ltr  Label        Fs     Type        Size     Status     Info
    ----------  ---  -----------  -----  ----------  -------  ---------  --------
    Volume 0     F                       CD-ROM          0 B  No Media
    Volume 1     C                NTFS   Partition     49 GB  Healthy    Boot
    Volume 2                      NTFS   Partition    505 MB  Healthy
    Volume 3                      FAT32  Partition    100 MB  Healthy    System
  ````

  2. Use diskpart to assign a drive letters to Windows and the EFI volume:
  ````
  list volume
  select volume #
  assign letter=v:
  select volume #
  assign letter=c:
  ````

  4. Format the EFI volume:
  ````
  format v: /fs:fat32
  ````

  5. Copy the EFI files back to the EFI volume:
  ````
  bcdboot c:\windows /s v: /f UEFI
  ````

### UEFI/GPT hard drive partitions

This shows the basic partition layout for Windows using GPT

  1. System partition:
  ````
  create partition efi size=100
  format quick fs=fat32 label="System"
  ````
  
  2. Microsoft Reserved (MSR) partition:
  ````
  create partition msr size=16
  ````
  
  3. Windows partition (The shrink is to make room for recovery tools):
  ````
  create partition primary
  shrink minimum=650
  format quick fs=ntfs label="Windows"
  ````
  
  4. Recovery tools partition:
  ````
  create partition primary
  format quick fs=ntfs label="Recovery tools"
  set id="de94bba4-06d1-4d40-a16a-bfd50179d6ac"
  gpt attributes=0x8000000000000001
  ````

### BIOS/MBR hard drive partitions

This shows the basic partition layout for Windows using MBR

  1. System partition:
  ````
  create partition primary size=100
  format quick fs=ntfs label="System"
  ````
  
  2. Windows partition (The shrink is to make room for recovery tools):
  ````
  create partition primary
  shrink minimum=650
  format quick fs=ntfs label="Windows"
  ````
  
  3. Recovery tools partition:
  ````
  create partition primary
  format quick fs=ntfs label="Recovery"
  set id=27
  ````
