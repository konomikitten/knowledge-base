# Power

  * [Find programs preventing monitor sleep](#Find-programs-preventing-monitor-sleep)

### Find programs preventing monitor sleep

  1. Open a command prompt with administrator privledges.
  
  2. Check requests with powercfg:
  ````
  powercfg /requests
  ````
  
  3. If you can't figure out what is preventing the monitor from sleeping try doing a trace instead:
  ````
  powercfg -energy -trace
  ````
  
  4. Open the resulting log file with event viewer the log is located in:
  ````
  C:\Windows\System32\energytrace.etl
  ````
